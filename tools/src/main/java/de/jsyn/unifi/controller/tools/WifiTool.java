package de.jsyn.unifi.controller.tools;

import de.jsyn.unifi.controller.client.UnifiClient;

import java.util.List;

class WifiTool {

    private UnifiClient client;

    WifiTool(String url, String username, String password){
        client = new UnifiClient(url).login(username, password);
    }

    void updatePassword(String site, String wifiName, String password) {
        client.updatePassword(site, wifiName, password);
    }

    List<String> getWifiList(String site) {
        return client.getWifiNameList(site);
    }
}
