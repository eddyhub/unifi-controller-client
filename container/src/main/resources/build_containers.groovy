class BuildContainer {

    def containerRegistry = ""
    static final def ARCH = ['amd64': [os: 'linux', arch: 'amd64'], 'arm': [os: 'linux', arch: 'arm', variant: 'v7'], 'arm64': [os: 'linux', arch: 'arm64', variant: 'v8']]
    def manifestName
    def manifestTransport
    def version
    def basedir
    def workdir
    def containerName

    BuildContainer(File basedir, String version, String containerName) {
        this.basedir = basedir
        this.workdir = new File(basedir, "target")
        this.version = version
        this.containerName = containerName
        if(System.getenv("CONTAINER_REGISTRY") != null) {
            containerRegistry = System.getenv("CONTAINER_REGISTRY")
        }
        manifestName = "${containerName}:${version}"
        manifestTransport= "docker://${containerRegistry}/${manifestName}"
    }

    void execute(String command, int timeout = 60000) {
        def stdout = new StringBuilder()
        def stderr = new StringBuilder()
        def proc = command.execute(null, workdir)
        proc.consumeProcessOutput(stdout, stderr)
        proc.waitForOrKill(timeout)
        System.out.println(stdout)
        System.out.println(stderr)
    }

    void buildContainerImages() {
        ARCH.each { k, v -> execute("buildah bud --manifest=${manifestName} --os=${v.os} --arch=${v.arch} --variant=${v.variant ?: "" } --tag=${containerRegistry}/${containerName}:${version} .")}
    }

    void buildContainerManifest() {
        System.out.println("")
        execute("buildah manifest rm ${manifestName}")
        execute("buildah manifest create ${manifestName}")
    }

    void pushContainerManifest() {
        if(containerRegistry.length() > 0) {
            execute("buildah manifest push --all ${manifestName} ${manifestTransport}")
        }
    }
}

def container = new BuildContainer(project.getBasedir(), project.getVersion(), project.getName())
if (properties.phase == "deploy") {
    container.pushContainerManifest()
} else {
    container.buildContainerManifest()
    container.buildContainerImages()
}
