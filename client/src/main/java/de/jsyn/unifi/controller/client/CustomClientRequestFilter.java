package de.jsyn.unifi.controller.client;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;
import java.util.function.Supplier;

public class CustomClientRequestFilter implements ClientRequestFilter {

    private Supplier<String> tokenSupplier;
    private static final String X_CSRF_TOKEN = "X-CSRF-Token";

    public CustomClientRequestFilter(Supplier<String> tokenSupplier) {
        this.tokenSupplier = tokenSupplier;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        if (tokenSupplier.get() != null) {
            requestContext.getHeaders().putSingle(X_CSRF_TOKEN, tokenSupplier.get());
        }
    }
}
