package de.jsyn.unifi.controller.client;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.jsyn.unifi.controller.api.ApiApi;
import de.jsyn.unifi.controller.api.ProxyApi;
import de.jsyn.unifi.controller.model.Login;
import de.jsyn.unifi.controller.model.LoginResponse;
import de.jsyn.unifi.controller.model.WlanConf;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UnifiClient {

    private final ResteasyWebTarget target;

    public UnifiClient(String url) {
        CustomResteasyJackson2Proider resteasyJackson2Provider = new CustomResteasyJackson2Proider();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        resteasyJackson2Provider.setMapper(objectMapper);

        CustomClientResponseFilter clientResponseFilter = new CustomClientResponseFilter();
        CustomClientRequestFilter clientRequestFilter = new CustomClientRequestFilter(clientResponseFilter::getxCsrfToken);

        Client client = ((ResteasyClientBuilder) ClientBuilder.newBuilder()).enableCookieManagement().register(resteasyJackson2Provider).register(clientResponseFilter).register(clientRequestFilter).build();
        target = (ResteasyWebTarget) client.target(url);
    }

    public UnifiClient login(String username, String password) {
        loginWithResponse(username, password);
        return this;
    }

    protected LoginResponse loginWithResponse(String username, String password) {
        Login login = new Login().username(username).password(password).token("").rememberMe(false);
        return this.getAuthApi().login(login);
    }

    private ApiApi getAuthApi() {
        return target.proxy(ApiApi.class);
    }

    private ProxyApi getSApi() {
        return target.proxy(ProxyApi.class);
    }

    public List<String> getWifiNameList(String site) {
        return getWifiConfList(site).stream()
                .map(WlanConf::getName)
                .collect(Collectors.toList());
    }

    private List<WlanConf> getWifiConfList(String site) {
        return getSApi().listWlanConfigs(site).getData();
    }

    public void updatePassword(String site, String wifiName, String password) {
        final Optional<WlanConf> wlanConf = getWifiConfList(site).stream()
                .filter(c -> wifiName.equals(c.getName())).findFirst();

        wlanConf.ifPresent(conf -> {
            conf.setxPassphrase(password);
            getSApi().updateWlanConfig(site, conf.getId(), conf);
        });
    }

    public String getWifiPassword(String site, String wifiName) {
        final Optional<WlanConf> wlanConf = getWifiConfList(site).stream()
                .filter(c -> wifiName.equals(c.getName())).findFirst();
        return wlanConf.map(WlanConf::getxPassphrase).orElse(null);
    }
}
