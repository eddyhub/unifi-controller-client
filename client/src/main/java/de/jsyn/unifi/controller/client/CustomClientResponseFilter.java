package de.jsyn.unifi.controller.client;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomClientResponseFilter implements ClientResponseFilter {

    private final Logger LOG = Logger.getLogger(CustomClientResponseFilter.class.getName());
    private static final String X_CSRF_TOKEN = "X-CSRF-Token";
    private String xCsrfToken;

    public String getxCsrfToken() {
        return xCsrfToken;
    }

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        final String token = responseContext.getHeaderString(X_CSRF_TOKEN);
        if (responseContext.getStatusInfo().getFamily().equals(Response.Status.Family.CLIENT_ERROR)) {
            String errorMessage =  new String(responseContext.getEntityStream().readAllBytes(), StandardCharsets.UTF_8);
            LOG.log(Level.ALL, "Error: " + errorMessage);
        }
        if (token != null) {
            xCsrfToken = token;
        }
    }
}
