package de.jsyn.unifi.controller.client;

import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import de.jsyn.unifi.controller.model.LoginResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.util.List;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;

class UnifiClientTest {
    private static final String USERNAME = "foo";
    private static final String PASSWORD = "bar";
    private static final String SITE = "default";
    private static final String WIFI_NAME = "alderaan";
    private static final String OLD_WIFI_PASSWORD = "my_secret_password";
    private static final String NEW_WIFI_PASSWORD = "my_new_secret_password";
    private static UnifiClient client;

    @RegisterExtension
    static final WireMockExtension wireMockExtension = WireMockExtension.newInstance()
            .options(wireMockConfig().dynamicPort())
            .build();

    @BeforeAll
    static void init() {
        System.setProperty("org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY", "Info");
        String url = wireMockExtension.baseUrl();
        client = new UnifiClient(url);
        final LoginResponse loginResponse = client.loginWithResponse(USERNAME, PASSWORD);
        assertEquals(USERNAME, loginResponse.getUsername());
    }

    @Test
    void should_return_list_of_wifi_names_from_default_site() {
        final List<String> wifiNameList = client.getWifiNameList(SITE);
        assertEquals(2, wifiNameList.size());
        assertLinesMatch(wifiNameList, List.of(WIFI_NAME, "tatooine"));
    }

    @Test
    void should_change_password_of_one_wifi() {
        wireMockExtension.resetScenarios();
        String old_password = client.getWifiPassword(SITE, WIFI_NAME);
        client.updatePassword(SITE, WIFI_NAME, NEW_WIFI_PASSWORD);
        String new_password = client.getWifiPassword(SITE, WIFI_NAME);
        assertEquals(OLD_WIFI_PASSWORD, old_password);
        assertEquals(NEW_WIFI_PASSWORD, new_password);
    }
}
