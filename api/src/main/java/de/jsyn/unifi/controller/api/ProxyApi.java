package de.jsyn.unifi.controller.api;

import de.jsyn.unifi.controller.model.WlanConf;
import de.jsyn.unifi.controller.model.WlanConfs;
import de.jsyn.unifi.controller.model.WlanGroups;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/proxy/network/api/s/{site}/rest")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public interface ProxyApi {

    @GET
    @Path("/wlanconf/{confId}")
    @Produces({ "application/json" })
    WlanConfs listWlanConfig(@PathParam("site") String site,@PathParam("confId") String confId);

    @GET
    @Path("/wlanconf")
    @Produces({ "application/json" })
    WlanConfs listWlanConfigs(@PathParam("site") String site);

    @GET
    @Path("/wlangroup")
    @Produces({ "application/json" })
    WlanGroups listWlanGroups(@PathParam("site") String site);

    @PUT
    @Path("/wlanconf/{confId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    WlanConfs updateWlanConfig(@PathParam("site") String site,@PathParam("confId") String confId,@Valid @NotNull WlanConf wlanConf);
}
