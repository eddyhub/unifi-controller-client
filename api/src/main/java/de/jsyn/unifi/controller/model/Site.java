package de.jsyn.unifi.controller.model;

import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("Site")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class Site  implements Serializable {
  
  private @Valid String id;
  private @Valid String attrHiddenId;
  private @Valid Boolean attrNoDelete;
  private @Valid String desc;
  private @Valid String name;
  private @Valid String role;

  /**
   **/
  public Site id(String id) {
    this.id = id;
    return this;
  }

  
  @JsonProperty("_id")
  @NotNull
  public String getId() {
    return id;
  }

  @JsonProperty("_id")
  public void setId(String id) {
    this.id = id;
  }

/**
   **/
  public Site attrHiddenId(String attrHiddenId) {
    this.attrHiddenId = attrHiddenId;
    return this;
  }

  
  @JsonProperty("attr_hidden_id")
  public String getAttrHiddenId() {
    return attrHiddenId;
  }

  @JsonProperty("attr_hidden_id")
  public void setAttrHiddenId(String attrHiddenId) {
    this.attrHiddenId = attrHiddenId;
  }

/**
   **/
  public Site attrNoDelete(Boolean attrNoDelete) {
    this.attrNoDelete = attrNoDelete;
    return this;
  }

  
  @JsonProperty("attr_no_delete")
  public Boolean getAttrNoDelete() {
    return attrNoDelete;
  }

  @JsonProperty("attr_no_delete")
  public void setAttrNoDelete(Boolean attrNoDelete) {
    this.attrNoDelete = attrNoDelete;
  }

/**
   **/
  public Site desc(String desc) {
    this.desc = desc;
    return this;
  }

  
  @JsonProperty("desc")
  public String getDesc() {
    return desc;
  }

  @JsonProperty("desc")
  public void setDesc(String desc) {
    this.desc = desc;
  }

/**
   **/
  public Site name(String name) {
    this.name = name;
    return this;
  }

  
  @JsonProperty("name")
  @NotNull
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

/**
   **/
  public Site role(String role) {
    this.role = role;
    return this;
  }

  
  @JsonProperty("role")
  public String getRole() {
    return role;
  }

  @JsonProperty("role")
  public void setRole(String role) {
    this.role = role;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Site site = (Site) o;
    return Objects.equals(this.id, site.id) &&
        Objects.equals(this.attrHiddenId, site.attrHiddenId) &&
        Objects.equals(this.attrNoDelete, site.attrNoDelete) &&
        Objects.equals(this.desc, site.desc) &&
        Objects.equals(this.name, site.name) &&
        Objects.equals(this.role, site.role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, attrHiddenId, attrNoDelete, desc, name, role);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Site {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    attrHiddenId: ").append(toIndentedString(attrHiddenId)).append("\n");
    sb.append("    attrNoDelete: ").append(toIndentedString(attrNoDelete)).append("\n");
    sb.append("    desc: ").append(toIndentedString(desc)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

