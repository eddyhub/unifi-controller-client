package de.jsyn.unifi.controller.api;

import de.jsyn.unifi.controller.model.Sites;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/self/sites")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public interface SelfApi {

    @GET
    @Produces({ "application/json" })
    Sites listSites();
}
