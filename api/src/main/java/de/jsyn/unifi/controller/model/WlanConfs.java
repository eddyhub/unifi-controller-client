package de.jsyn.unifi.controller.model;

import de.jsyn.unifi.controller.model.Meta;
import de.jsyn.unifi.controller.model.WlanConf;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("WlanConfs")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class WlanConfs  implements Serializable {
  
  private @Valid List<WlanConf> data = new ArrayList<>();
  private @Valid Meta meta;

  /**
   **/
  public WlanConfs data(List<WlanConf> data) {
    this.data = data;
    return this;
  }

  
  @JsonProperty("data")
  @NotNull
  public List<WlanConf> getData() {
    return data;
  }

  @JsonProperty("data")
  public void setData(List<WlanConf> data) {
    this.data = data;
  }

  public WlanConfs addDataItem(WlanConf dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }

    this.data.add(dataItem);
    return this;
  }

  public WlanConfs removeDataItem(WlanConf dataItem) {
    if (dataItem != null && this.data != null) {
      this.data.remove(dataItem);
    }

    return this;
  }
/**
   **/
  public WlanConfs meta(Meta meta) {
    this.meta = meta;
    return this;
  }

  
  @JsonProperty("meta")
  @NotNull
  public Meta getMeta() {
    return meta;
  }

  @JsonProperty("meta")
  public void setMeta(Meta meta) {
    this.meta = meta;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WlanConfs wlanConfs = (WlanConfs) o;
    return Objects.equals(this.data, wlanConfs.data) &&
        Objects.equals(this.meta, wlanConfs.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, meta);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WlanConfs {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

