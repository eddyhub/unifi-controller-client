package de.jsyn.unifi.controller.model;

import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("WlanGroup")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class WlanGroup  implements Serializable {
  
  private @Valid String id;
  private @Valid String name;
  private @Valid String siteId;

  /**
   **/
  public WlanGroup id(String id) {
    this.id = id;
    return this;
  }

  
  @JsonProperty("_id")
  @NotNull
  public String getId() {
    return id;
  }

  @JsonProperty("_id")
  public void setId(String id) {
    this.id = id;
  }

/**
   **/
  public WlanGroup name(String name) {
    this.name = name;
    return this;
  }

  
  @JsonProperty("name")
  @NotNull
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

/**
   **/
  public WlanGroup siteId(String siteId) {
    this.siteId = siteId;
    return this;
  }

  
  @JsonProperty("site_id")
  @NotNull
  public String getSiteId() {
    return siteId;
  }

  @JsonProperty("site_id")
  public void setSiteId(String siteId) {
    this.siteId = siteId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WlanGroup wlanGroup = (WlanGroup) o;
    return Objects.equals(this.id, wlanGroup.id) &&
        Objects.equals(this.name, wlanGroup.name) &&
        Objects.equals(this.siteId, wlanGroup.siteId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, siteId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WlanGroup {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    siteId: ").append(toIndentedString(siteId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

