package de.jsyn.unifi.controller.api;

import de.jsyn.unifi.controller.model.Login;
import de.jsyn.unifi.controller.model.LoginResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;


import java.io.InputStream;
import java.util.Map;
import java.util.List;
import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/api/auth/login")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public interface ApiApi {

    @POST
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    LoginResponse login(@Valid Login login);
}
