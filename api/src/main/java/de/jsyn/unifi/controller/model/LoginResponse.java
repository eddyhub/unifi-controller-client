package de.jsyn.unifi.controller.model;

import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("LoginResponse")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class LoginResponse  implements Serializable {
  
  private @Valid String uniqueId;
  private @Valid String firstName;
  private @Valid String lastName;
  private @Valid String fullName;
  private @Valid String email;
  private @Valid String emailIsNull;
  private @Valid String phone;
  private @Valid String avatarRelativePath;
  private @Valid String avatarRpath2;
  private @Valid String status;
  private @Valid String employeeNumber;
  private @Valid Integer createTime;
  private @Valid String username;
  private @Valid Boolean localAccountExist;
  private @Valid Integer passwordRevision;
  private @Valid Integer updateTime;
  private @Valid String id;
  private @Valid Boolean isOwner;
  private @Valid Boolean isSuperAdmin;
  private @Valid Boolean isMember;
  private @Valid String deviceToken;

  /**
   **/
  public LoginResponse uniqueId(String uniqueId) {
    this.uniqueId = uniqueId;
    return this;
  }

  
  @JsonProperty("unique_id")
  @NotNull
  public String getUniqueId() {
    return uniqueId;
  }

  @JsonProperty("unique_id")
  public void setUniqueId(String uniqueId) {
    this.uniqueId = uniqueId;
  }

/**
   **/
  public LoginResponse firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  
  @JsonProperty("first_name")
  public String getFirstName() {
    return firstName;
  }

  @JsonProperty("first_name")
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

/**
   **/
  public LoginResponse lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  
  @JsonProperty("last_name")
  public String getLastName() {
    return lastName;
  }

  @JsonProperty("last_name")
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

/**
   **/
  public LoginResponse fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

  
  @JsonProperty("full_name")
  public String getFullName() {
    return fullName;
  }

  @JsonProperty("full_name")
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

/**
   **/
  public LoginResponse email(String email) {
    this.email = email;
    return this;
  }

  
  @JsonProperty("email")
  public String getEmail() {
    return email;
  }

  @JsonProperty("email")
  public void setEmail(String email) {
    this.email = email;
  }

/**
   **/
  public LoginResponse emailIsNull(String emailIsNull) {
    this.emailIsNull = emailIsNull;
    return this;
  }

  
  @JsonProperty("email_is_null")
  public String getEmailIsNull() {
    return emailIsNull;
  }

  @JsonProperty("email_is_null")
  public void setEmailIsNull(String emailIsNull) {
    this.emailIsNull = emailIsNull;
  }

/**
   **/
  public LoginResponse phone(String phone) {
    this.phone = phone;
    return this;
  }

  
  @JsonProperty("phone")
  public String getPhone() {
    return phone;
  }

  @JsonProperty("phone")
  public void setPhone(String phone) {
    this.phone = phone;
  }

/**
   **/
  public LoginResponse avatarRelativePath(String avatarRelativePath) {
    this.avatarRelativePath = avatarRelativePath;
    return this;
  }

  
  @JsonProperty("avatar_relative_path")
  public String getAvatarRelativePath() {
    return avatarRelativePath;
  }

  @JsonProperty("avatar_relative_path")
  public void setAvatarRelativePath(String avatarRelativePath) {
    this.avatarRelativePath = avatarRelativePath;
  }

/**
   **/
  public LoginResponse avatarRpath2(String avatarRpath2) {
    this.avatarRpath2 = avatarRpath2;
    return this;
  }

  
  @JsonProperty("avatar_rpath2")
  public String getAvatarRpath2() {
    return avatarRpath2;
  }

  @JsonProperty("avatar_rpath2")
  public void setAvatarRpath2(String avatarRpath2) {
    this.avatarRpath2 = avatarRpath2;
  }

/**
   **/
  public LoginResponse status(String status) {
    this.status = status;
    return this;
  }

  
  @JsonProperty("status")
  public String getStatus() {
    return status;
  }

  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

/**
   **/
  public LoginResponse employeeNumber(String employeeNumber) {
    this.employeeNumber = employeeNumber;
    return this;
  }

  
  @JsonProperty("employee_number")
  public String getEmployeeNumber() {
    return employeeNumber;
  }

  @JsonProperty("employee_number")
  public void setEmployeeNumber(String employeeNumber) {
    this.employeeNumber = employeeNumber;
  }

/**
   **/
  public LoginResponse createTime(Integer createTime) {
    this.createTime = createTime;
    return this;
  }

  
  @JsonProperty("create_time")
  public Integer getCreateTime() {
    return createTime;
  }

  @JsonProperty("create_time")
  public void setCreateTime(Integer createTime) {
    this.createTime = createTime;
  }

/**
   **/
  public LoginResponse username(String username) {
    this.username = username;
    return this;
  }

  
  @JsonProperty("username")
  public String getUsername() {
    return username;
  }

  @JsonProperty("username")
  public void setUsername(String username) {
    this.username = username;
  }

/**
   **/
  public LoginResponse localAccountExist(Boolean localAccountExist) {
    this.localAccountExist = localAccountExist;
    return this;
  }

  
  @JsonProperty("local_account_exist")
  public Boolean getLocalAccountExist() {
    return localAccountExist;
  }

  @JsonProperty("local_account_exist")
  public void setLocalAccountExist(Boolean localAccountExist) {
    this.localAccountExist = localAccountExist;
  }

/**
   **/
  public LoginResponse passwordRevision(Integer passwordRevision) {
    this.passwordRevision = passwordRevision;
    return this;
  }

  
  @JsonProperty("password_revision")
  public Integer getPasswordRevision() {
    return passwordRevision;
  }

  @JsonProperty("password_revision")
  public void setPasswordRevision(Integer passwordRevision) {
    this.passwordRevision = passwordRevision;
  }

/**
   **/
  public LoginResponse updateTime(Integer updateTime) {
    this.updateTime = updateTime;
    return this;
  }

  
  @JsonProperty("update_time")
  public Integer getUpdateTime() {
    return updateTime;
  }

  @JsonProperty("update_time")
  public void setUpdateTime(Integer updateTime) {
    this.updateTime = updateTime;
  }

/**
   **/
  public LoginResponse id(String id) {
    this.id = id;
    return this;
  }

  
  @JsonProperty("id")
  @NotNull
  public String getId() {
    return id;
  }

  @JsonProperty("id")
  public void setId(String id) {
    this.id = id;
  }

/**
   **/
  public LoginResponse isOwner(Boolean isOwner) {
    this.isOwner = isOwner;
    return this;
  }

  
  @JsonProperty("isOwner")
  public Boolean getIsOwner() {
    return isOwner;
  }

  @JsonProperty("isOwner")
  public void setIsOwner(Boolean isOwner) {
    this.isOwner = isOwner;
  }

/**
   **/
  public LoginResponse isSuperAdmin(Boolean isSuperAdmin) {
    this.isSuperAdmin = isSuperAdmin;
    return this;
  }

  
  @JsonProperty("isSuperAdmin")
  public Boolean getIsSuperAdmin() {
    return isSuperAdmin;
  }

  @JsonProperty("isSuperAdmin")
  public void setIsSuperAdmin(Boolean isSuperAdmin) {
    this.isSuperAdmin = isSuperAdmin;
  }

/**
   **/
  public LoginResponse isMember(Boolean isMember) {
    this.isMember = isMember;
    return this;
  }

  
  @JsonProperty("isMember")
  public Boolean getIsMember() {
    return isMember;
  }

  @JsonProperty("isMember")
  public void setIsMember(Boolean isMember) {
    this.isMember = isMember;
  }

/**
   **/
  public LoginResponse deviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
    return this;
  }

  
  @JsonProperty("deviceToken")
  @NotNull
  public String getDeviceToken() {
    return deviceToken;
  }

  @JsonProperty("deviceToken")
  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoginResponse loginResponse = (LoginResponse) o;
    return Objects.equals(this.uniqueId, loginResponse.uniqueId) &&
        Objects.equals(this.firstName, loginResponse.firstName) &&
        Objects.equals(this.lastName, loginResponse.lastName) &&
        Objects.equals(this.fullName, loginResponse.fullName) &&
        Objects.equals(this.email, loginResponse.email) &&
        Objects.equals(this.emailIsNull, loginResponse.emailIsNull) &&
        Objects.equals(this.phone, loginResponse.phone) &&
        Objects.equals(this.avatarRelativePath, loginResponse.avatarRelativePath) &&
        Objects.equals(this.avatarRpath2, loginResponse.avatarRpath2) &&
        Objects.equals(this.status, loginResponse.status) &&
        Objects.equals(this.employeeNumber, loginResponse.employeeNumber) &&
        Objects.equals(this.createTime, loginResponse.createTime) &&
        Objects.equals(this.username, loginResponse.username) &&
        Objects.equals(this.localAccountExist, loginResponse.localAccountExist) &&
        Objects.equals(this.passwordRevision, loginResponse.passwordRevision) &&
        Objects.equals(this.updateTime, loginResponse.updateTime) &&
        Objects.equals(this.id, loginResponse.id) &&
        Objects.equals(this.isOwner, loginResponse.isOwner) &&
        Objects.equals(this.isSuperAdmin, loginResponse.isSuperAdmin) &&
        Objects.equals(this.isMember, loginResponse.isMember) &&
        Objects.equals(this.deviceToken, loginResponse.deviceToken);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uniqueId, firstName, lastName, fullName, email, emailIsNull, phone, avatarRelativePath, avatarRpath2, status, employeeNumber, createTime, username, localAccountExist, passwordRevision, updateTime, id, isOwner, isSuperAdmin, isMember, deviceToken);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LoginResponse {\n");
    
    sb.append("    uniqueId: ").append(toIndentedString(uniqueId)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    emailIsNull: ").append(toIndentedString(emailIsNull)).append("\n");
    sb.append("    phone: ").append(toIndentedString(phone)).append("\n");
    sb.append("    avatarRelativePath: ").append(toIndentedString(avatarRelativePath)).append("\n");
    sb.append("    avatarRpath2: ").append(toIndentedString(avatarRpath2)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    employeeNumber: ").append(toIndentedString(employeeNumber)).append("\n");
    sb.append("    createTime: ").append(toIndentedString(createTime)).append("\n");
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("    localAccountExist: ").append(toIndentedString(localAccountExist)).append("\n");
    sb.append("    passwordRevision: ").append(toIndentedString(passwordRevision)).append("\n");
    sb.append("    updateTime: ").append(toIndentedString(updateTime)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    isOwner: ").append(toIndentedString(isOwner)).append("\n");
    sb.append("    isSuperAdmin: ").append(toIndentedString(isSuperAdmin)).append("\n");
    sb.append("    isMember: ").append(toIndentedString(isMember)).append("\n");
    sb.append("    deviceToken: ").append(toIndentedString(deviceToken)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

