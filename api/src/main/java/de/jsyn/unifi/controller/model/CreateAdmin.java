package de.jsyn.unifi.controller.model;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("CreateAdmin")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class CreateAdmin  implements Serializable {
  
  private @Valid String cmd;
  private @Valid String email;
  private @Valid String name;
  private @Valid Boolean requiredNewPassword;
  private @Valid String role;
  private @Valid String xPassword;

public enum PermissionsEnum {

    ADOPT(String.valueOf("API_DEVICE_ADOPT")), RESTART(String.valueOf("API_DEVICE_RESTART"));


    private String value;

    PermissionsEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Convert a String into String, as specified in the
     * <a href="https://download.oracle.com/otndocs/jcp/jaxrs-2_0-fr-eval-spec/index.html">See JAX RS 2.0 Specification, section 3.2, p. 12</a>
     */
	public static PermissionsEnum fromString(String s) {
        for (PermissionsEnum b : PermissionsEnum.values()) {
            // using Objects.toString() to be safe if value type non-object type
            // because types like 'int' etc. will be auto-boxed
            if (java.util.Objects.toString(b.value).equals(s)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected string value '" + s + "'");
	}
	
    @JsonCreator
    public static PermissionsEnum fromValue(String value) {
        for (PermissionsEnum b : PermissionsEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}

  private @Valid List<PermissionsEnum> permissions = new ArrayList<>();

  /**
   **/
  public CreateAdmin cmd(String cmd) {
    this.cmd = cmd;
    return this;
  }

  
  @JsonProperty("cmd")
  @NotNull
  public String getCmd() {
    return cmd;
  }

  @JsonProperty("cmd")
  public void setCmd(String cmd) {
    this.cmd = cmd;
  }

/**
   **/
  public CreateAdmin email(String email) {
    this.email = email;
    return this;
  }

  
  @JsonProperty("email")
  @NotNull
  public String getEmail() {
    return email;
  }

  @JsonProperty("email")
  public void setEmail(String email) {
    this.email = email;
  }

/**
   **/
  public CreateAdmin name(String name) {
    this.name = name;
    return this;
  }

  
  @JsonProperty("name")
  @NotNull
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

/**
   **/
  public CreateAdmin requiredNewPassword(Boolean requiredNewPassword) {
    this.requiredNewPassword = requiredNewPassword;
    return this;
  }

  
  @JsonProperty("required_new_password")
  @NotNull
  public Boolean getRequiredNewPassword() {
    return requiredNewPassword;
  }

  @JsonProperty("required_new_password")
  public void setRequiredNewPassword(Boolean requiredNewPassword) {
    this.requiredNewPassword = requiredNewPassword;
  }

/**
   **/
  public CreateAdmin role(String role) {
    this.role = role;
    return this;
  }

  
  @JsonProperty("role")
  @NotNull
  public String getRole() {
    return role;
  }

  @JsonProperty("role")
  public void setRole(String role) {
    this.role = role;
  }

/**
   **/
  public CreateAdmin xPassword(String xPassword) {
    this.xPassword = xPassword;
    return this;
  }

  
  @JsonProperty("x_password")
  @NotNull
  public String getxPassword() {
    return xPassword;
  }

  @JsonProperty("x_password")
  public void setxPassword(String xPassword) {
    this.xPassword = xPassword;
  }

/**
   **/
  public CreateAdmin permissions(List<PermissionsEnum> permissions) {
    this.permissions = permissions;
    return this;
  }

  
  @JsonProperty("permissions")
  @NotNull
  public List<PermissionsEnum> getPermissions() {
    return permissions;
  }

  @JsonProperty("permissions")
  public void setPermissions(List<PermissionsEnum> permissions) {
    this.permissions = permissions;
  }

  public CreateAdmin addPermissionsItem(PermissionsEnum permissionsItem) {
    if (this.permissions == null) {
      this.permissions = new ArrayList<>();
    }

    this.permissions.add(permissionsItem);
    return this;
  }

  public CreateAdmin removePermissionsItem(PermissionsEnum permissionsItem) {
    if (permissionsItem != null && this.permissions != null) {
      this.permissions.remove(permissionsItem);
    }

    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateAdmin createAdmin = (CreateAdmin) o;
    return Objects.equals(this.cmd, createAdmin.cmd) &&
        Objects.equals(this.email, createAdmin.email) &&
        Objects.equals(this.name, createAdmin.name) &&
        Objects.equals(this.requiredNewPassword, createAdmin.requiredNewPassword) &&
        Objects.equals(this.role, createAdmin.role) &&
        Objects.equals(this.xPassword, createAdmin.xPassword) &&
        Objects.equals(this.permissions, createAdmin.permissions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cmd, email, name, requiredNewPassword, role, xPassword, permissions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateAdmin {\n");
    
    sb.append("    cmd: ").append(toIndentedString(cmd)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    requiredNewPassword: ").append(toIndentedString(requiredNewPassword)).append("\n");
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    xPassword: ").append(toIndentedString(xPassword)).append("\n");
    sb.append("    permissions: ").append(toIndentedString(permissions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

