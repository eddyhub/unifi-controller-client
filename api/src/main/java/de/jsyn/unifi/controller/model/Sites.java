package de.jsyn.unifi.controller.model;

import de.jsyn.unifi.controller.model.Meta;
import de.jsyn.unifi.controller.model.Site;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("Sites")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class Sites  implements Serializable {
  
  private @Valid List<Site> data = new ArrayList<>();
  private @Valid Meta meta;

  /**
   **/
  public Sites data(List<Site> data) {
    this.data = data;
    return this;
  }

  
  @JsonProperty("data")
  @NotNull
  public List<Site> getData() {
    return data;
  }

  @JsonProperty("data")
  public void setData(List<Site> data) {
    this.data = data;
  }

  public Sites addDataItem(Site dataItem) {
    if (this.data == null) {
      this.data = new ArrayList<>();
    }

    this.data.add(dataItem);
    return this;
  }

  public Sites removeDataItem(Site dataItem) {
    if (dataItem != null && this.data != null) {
      this.data.remove(dataItem);
    }

    return this;
  }
/**
   **/
  public Sites meta(Meta meta) {
    this.meta = meta;
    return this;
  }

  
  @JsonProperty("meta")
  @NotNull
  public Meta getMeta() {
    return meta;
  }

  @JsonProperty("meta")
  public void setMeta(Meta meta) {
    this.meta = meta;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Sites sites = (Sites) o;
    return Objects.equals(this.data, sites.data) &&
        Objects.equals(this.meta, sites.meta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(data, meta);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Sites {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

