package de.jsyn.unifi.controller.model;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("WlanConf")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class WlanConf  implements Serializable {
  
  private @Valid String id;
  private @Valid Boolean bcFilterEnabled;
  private @Valid List<String> bcFilterList = new ArrayList<>();
  private @Valid String dtimMode;
  private @Valid Integer dtimNa;
  private @Valid Integer dtimNg;
  private @Valid Boolean enabled;
  private @Valid Integer groupRekey;
  private @Valid Boolean isGuest;
  private @Valid Boolean macFilterEnabled;
  private @Valid List<String> macFilterList = new ArrayList<>();
  private @Valid String macFilterPolicy;
  private @Valid Boolean minrateNaAdvertisingRates;
  private @Valid Integer minrateNaDataRateKbps;
  private @Valid Boolean minrateNaEnabled;
  private @Valid Boolean minrateNgAdvertisingRates;
  private @Valid Integer minrateNgDataRateKbps;
  private @Valid Boolean minrateNgEnabled;
  private @Valid String name;
  private @Valid List<String> schedule = new ArrayList<>();
  private @Valid Boolean scheduleEnabled;
  private @Valid Boolean scheduleReversed;
  private @Valid Boolean radiusDasEnabled;
  private @Valid Boolean hotspot2confEnabled;
  private @Valid Boolean bssTransition;
  private @Valid Boolean authCache;
  private @Valid String radiusprofileId;
  private @Valid String pmfMode;
  private @Valid Boolean bSupported;

public enum SecurityEnum {

    WPAPSK(String.valueOf("wpapsk")), WPAEAP(String.valueOf("wpaeap")), WEP(String.valueOf("wep"));


    private String value;

    SecurityEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Convert a String into String, as specified in the
     * <a href="https://download.oracle.com/otndocs/jcp/jaxrs-2_0-fr-eval-spec/index.html">See JAX RS 2.0 Specification, section 3.2, p. 12</a>
     */
	public static SecurityEnum fromString(String s) {
        for (SecurityEnum b : SecurityEnum.values()) {
            // using Objects.toString() to be safe if value type non-object type
            // because types like 'int' etc. will be auto-boxed
            if (java.util.Objects.toString(b.value).equals(s)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected string value '" + s + "'");
	}
	
    @JsonCreator
    public static SecurityEnum fromValue(String value) {
        for (SecurityEnum b : SecurityEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}

  private @Valid SecurityEnum security = SecurityEnum.WPAPSK;
  private @Valid String siteId;
  private @Valid String usergroupId;
  private @Valid Integer wepIdx;

public enum WpaEncEnum {

    CCMP(String.valueOf("ccmp"));


    private String value;

    WpaEncEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Convert a String into String, as specified in the
     * <a href="https://download.oracle.com/otndocs/jcp/jaxrs-2_0-fr-eval-spec/index.html">See JAX RS 2.0 Specification, section 3.2, p. 12</a>
     */
	public static WpaEncEnum fromString(String s) {
        for (WpaEncEnum b : WpaEncEnum.values()) {
            // using Objects.toString() to be safe if value type non-object type
            // because types like 'int' etc. will be auto-boxed
            if (java.util.Objects.toString(b.value).equals(s)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected string value '" + s + "'");
	}
	
    @JsonCreator
    public static WpaEncEnum fromValue(String value) {
        for (WpaEncEnum b : WpaEncEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}

  private @Valid WpaEncEnum wpaEnc = WpaEncEnum.CCMP;

public enum WpaModeEnum {

    WPA2(String.valueOf("wpa2"));


    private String value;

    WpaModeEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Convert a String into String, as specified in the
     * <a href="https://download.oracle.com/otndocs/jcp/jaxrs-2_0-fr-eval-spec/index.html">See JAX RS 2.0 Specification, section 3.2, p. 12</a>
     */
	public static WpaModeEnum fromString(String s) {
        for (WpaModeEnum b : WpaModeEnum.values()) {
            // using Objects.toString() to be safe if value type non-object type
            // because types like 'int' etc. will be auto-boxed
            if (java.util.Objects.toString(b.value).equals(s)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected string value '" + s + "'");
	}
	
    @JsonCreator
    public static WpaModeEnum fromValue(String value) {
        for (WpaModeEnum b : WpaModeEnum.values()) {
            if (b.value.equals(value)) {
                return b;
            }
        }
        throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
}

  private @Valid WpaModeEnum wpaMode = WpaModeEnum.WPA2;
  private @Valid String xIappKey;
  private @Valid String xPassphrase;
  private @Valid List<String> apGroupIds = new ArrayList<>();
  private @Valid String wlanBand;
  private @Valid String networkconfId;
  private @Valid Boolean iappEnabled;
  private @Valid List<String> scheduleWithDuration = new ArrayList<>();
  private @Valid Boolean optimizeIotWifiConnectivity;
  private @Valid Integer dtim6e;
  private @Valid List<String> wlanBands = new ArrayList<>();
  private @Valid String settingPreference;
  private @Valid String minrateSettingPreference;
  private @Valid Boolean fastRoamingEnabled;
  private @Valid Boolean hideSsid;
  private @Valid Boolean l2Isolation;
  private @Valid Boolean mcastenhanceEnabled;
  private @Valid Boolean no2ghzOui;
  private @Valid Boolean proxyArp;
  private @Valid Boolean radiusMacAuthEnabled;
  private @Valid String radiusMacaclFormat;
  private @Valid List<String> saeGroups = new ArrayList<>();
  private @Valid List<String> saePsk = new ArrayList<>();
  private @Valid Boolean uapsdEnabled;
  private @Valid Boolean wpa3Enhanced192;
  private @Valid Boolean wpa3FastRoaming;
  private @Valid Boolean wpa3Support;
  private @Valid Boolean wpa3Transition;

  /**
   **/
  public WlanConf id(String id) {
    this.id = id;
    return this;
  }

  
  @JsonProperty("_id")
  public String getId() {
    return id;
  }

  @JsonProperty("_id")
  public void setId(String id) {
    this.id = id;
  }

/**
   **/
  public WlanConf bcFilterEnabled(Boolean bcFilterEnabled) {
    this.bcFilterEnabled = bcFilterEnabled;
    return this;
  }

  
  @JsonProperty("bc_filter_enabled")
  public Boolean getBcFilterEnabled() {
    return bcFilterEnabled;
  }

  @JsonProperty("bc_filter_enabled")
  public void setBcFilterEnabled(Boolean bcFilterEnabled) {
    this.bcFilterEnabled = bcFilterEnabled;
  }

/**
   **/
  public WlanConf bcFilterList(List<String> bcFilterList) {
    this.bcFilterList = bcFilterList;
    return this;
  }

  
  @JsonProperty("bc_filter_list")
  public List< @Pattern(regexp="((\\d|([a-f]|[A-F])){2}:){5}(\\d|([a-f]|[A-F])){2}")String> getBcFilterList() {
    return bcFilterList;
  }

  @JsonProperty("bc_filter_list")
  public void setBcFilterList(List<String> bcFilterList) {
    this.bcFilterList = bcFilterList;
  }

  public WlanConf addBcFilterListItem(String bcFilterListItem) {
    if (this.bcFilterList == null) {
      this.bcFilterList = new ArrayList<>();
    }

    this.bcFilterList.add(bcFilterListItem);
    return this;
  }

  public WlanConf removeBcFilterListItem(String bcFilterListItem) {
    if (bcFilterListItem != null && this.bcFilterList != null) {
      this.bcFilterList.remove(bcFilterListItem);
    }

    return this;
  }
/**
   **/
  public WlanConf dtimMode(String dtimMode) {
    this.dtimMode = dtimMode;
    return this;
  }

  
  @JsonProperty("dtim_mode")
  public String getDtimMode() {
    return dtimMode;
  }

  @JsonProperty("dtim_mode")
  public void setDtimMode(String dtimMode) {
    this.dtimMode = dtimMode;
  }

/**
   **/
  public WlanConf dtimNa(Integer dtimNa) {
    this.dtimNa = dtimNa;
    return this;
  }

  
  @JsonProperty("dtim_na")
  public Integer getDtimNa() {
    return dtimNa;
  }

  @JsonProperty("dtim_na")
  public void setDtimNa(Integer dtimNa) {
    this.dtimNa = dtimNa;
  }

/**
   **/
  public WlanConf dtimNg(Integer dtimNg) {
    this.dtimNg = dtimNg;
    return this;
  }

  
  @JsonProperty("dtim_ng")
  public Integer getDtimNg() {
    return dtimNg;
  }

  @JsonProperty("dtim_ng")
  public void setDtimNg(Integer dtimNg) {
    this.dtimNg = dtimNg;
  }

/**
   **/
  public WlanConf enabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  
  @JsonProperty("enabled")
  @NotNull
  public Boolean getEnabled() {
    return enabled;
  }

  @JsonProperty("enabled")
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

/**
   **/
  public WlanConf groupRekey(Integer groupRekey) {
    this.groupRekey = groupRekey;
    return this;
  }

  
  @JsonProperty("group_rekey")
  public Integer getGroupRekey() {
    return groupRekey;
  }

  @JsonProperty("group_rekey")
  public void setGroupRekey(Integer groupRekey) {
    this.groupRekey = groupRekey;
  }

/**
   **/
  public WlanConf isGuest(Boolean isGuest) {
    this.isGuest = isGuest;
    return this;
  }

  
  @JsonProperty("is_guest")
  public Boolean getIsGuest() {
    return isGuest;
  }

  @JsonProperty("is_guest")
  public void setIsGuest(Boolean isGuest) {
    this.isGuest = isGuest;
  }

/**
   **/
  public WlanConf macFilterEnabled(Boolean macFilterEnabled) {
    this.macFilterEnabled = macFilterEnabled;
    return this;
  }

  
  @JsonProperty("mac_filter_enabled")
  public Boolean getMacFilterEnabled() {
    return macFilterEnabled;
  }

  @JsonProperty("mac_filter_enabled")
  public void setMacFilterEnabled(Boolean macFilterEnabled) {
    this.macFilterEnabled = macFilterEnabled;
  }

/**
   **/
  public WlanConf macFilterList(List<String> macFilterList) {
    this.macFilterList = macFilterList;
    return this;
  }

  
  @JsonProperty("mac_filter_list")
  public List< @Pattern(regexp="((\\d|([a-f]|[A-F])){2}:){5}(\\d|([a-f]|[A-F])){2}")String> getMacFilterList() {
    return macFilterList;
  }

  @JsonProperty("mac_filter_list")
  public void setMacFilterList(List<String> macFilterList) {
    this.macFilterList = macFilterList;
  }

  public WlanConf addMacFilterListItem(String macFilterListItem) {
    if (this.macFilterList == null) {
      this.macFilterList = new ArrayList<>();
    }

    this.macFilterList.add(macFilterListItem);
    return this;
  }

  public WlanConf removeMacFilterListItem(String macFilterListItem) {
    if (macFilterListItem != null && this.macFilterList != null) {
      this.macFilterList.remove(macFilterListItem);
    }

    return this;
  }
/**
   **/
  public WlanConf macFilterPolicy(String macFilterPolicy) {
    this.macFilterPolicy = macFilterPolicy;
    return this;
  }

  
  @JsonProperty("mac_filter_policy")
  public String getMacFilterPolicy() {
    return macFilterPolicy;
  }

  @JsonProperty("mac_filter_policy")
  public void setMacFilterPolicy(String macFilterPolicy) {
    this.macFilterPolicy = macFilterPolicy;
  }

/**
   **/
  public WlanConf minrateNaAdvertisingRates(Boolean minrateNaAdvertisingRates) {
    this.minrateNaAdvertisingRates = minrateNaAdvertisingRates;
    return this;
  }

  
  @JsonProperty("minrate_na_advertising_rates")
  public Boolean getMinrateNaAdvertisingRates() {
    return minrateNaAdvertisingRates;
  }

  @JsonProperty("minrate_na_advertising_rates")
  public void setMinrateNaAdvertisingRates(Boolean minrateNaAdvertisingRates) {
    this.minrateNaAdvertisingRates = minrateNaAdvertisingRates;
  }

/**
   **/
  public WlanConf minrateNaDataRateKbps(Integer minrateNaDataRateKbps) {
    this.minrateNaDataRateKbps = minrateNaDataRateKbps;
    return this;
  }

  
  @JsonProperty("minrate_na_data_rate_kbps")
  public Integer getMinrateNaDataRateKbps() {
    return minrateNaDataRateKbps;
  }

  @JsonProperty("minrate_na_data_rate_kbps")
  public void setMinrateNaDataRateKbps(Integer minrateNaDataRateKbps) {
    this.minrateNaDataRateKbps = minrateNaDataRateKbps;
  }

/**
   **/
  public WlanConf minrateNaEnabled(Boolean minrateNaEnabled) {
    this.minrateNaEnabled = minrateNaEnabled;
    return this;
  }

  
  @JsonProperty("minrate_na_enabled")
  public Boolean getMinrateNaEnabled() {
    return minrateNaEnabled;
  }

  @JsonProperty("minrate_na_enabled")
  public void setMinrateNaEnabled(Boolean minrateNaEnabled) {
    this.minrateNaEnabled = minrateNaEnabled;
  }

/**
   **/
  public WlanConf minrateNgAdvertisingRates(Boolean minrateNgAdvertisingRates) {
    this.minrateNgAdvertisingRates = minrateNgAdvertisingRates;
    return this;
  }

  
  @JsonProperty("minrate_ng_advertising_rates")
  public Boolean getMinrateNgAdvertisingRates() {
    return minrateNgAdvertisingRates;
  }

  @JsonProperty("minrate_ng_advertising_rates")
  public void setMinrateNgAdvertisingRates(Boolean minrateNgAdvertisingRates) {
    this.minrateNgAdvertisingRates = minrateNgAdvertisingRates;
  }

/**
   **/
  public WlanConf minrateNgDataRateKbps(Integer minrateNgDataRateKbps) {
    this.minrateNgDataRateKbps = minrateNgDataRateKbps;
    return this;
  }

  
  @JsonProperty("minrate_ng_data_rate_kbps")
  public Integer getMinrateNgDataRateKbps() {
    return minrateNgDataRateKbps;
  }

  @JsonProperty("minrate_ng_data_rate_kbps")
  public void setMinrateNgDataRateKbps(Integer minrateNgDataRateKbps) {
    this.minrateNgDataRateKbps = minrateNgDataRateKbps;
  }

/**
   **/
  public WlanConf minrateNgEnabled(Boolean minrateNgEnabled) {
    this.minrateNgEnabled = minrateNgEnabled;
    return this;
  }

  
  @JsonProperty("minrate_ng_enabled")
  public Boolean getMinrateNgEnabled() {
    return minrateNgEnabled;
  }

  @JsonProperty("minrate_ng_enabled")
  public void setMinrateNgEnabled(Boolean minrateNgEnabled) {
    this.minrateNgEnabled = minrateNgEnabled;
  }

/**
   **/
  public WlanConf name(String name) {
    this.name = name;
    return this;
  }

  
  @JsonProperty("name")
  @NotNull
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

/**
   **/
  public WlanConf schedule(List<String> schedule) {
    this.schedule = schedule;
    return this;
  }

  
  @JsonProperty("schedule")
  public List< @Pattern(regexp="(mon|tue|wed|thu|fri|sat|sun)\\|(20|21|22|23|[0-1]\\d)[0-5]\\d-(20|21|22|23|[0-1]\\d)[0-5]\\d")String> getSchedule() {
    return schedule;
  }

  @JsonProperty("schedule")
  public void setSchedule(List<String> schedule) {
    this.schedule = schedule;
  }

  public WlanConf addScheduleItem(String scheduleItem) {
    if (this.schedule == null) {
      this.schedule = new ArrayList<>();
    }

    this.schedule.add(scheduleItem);
    return this;
  }

  public WlanConf removeScheduleItem(String scheduleItem) {
    if (scheduleItem != null && this.schedule != null) {
      this.schedule.remove(scheduleItem);
    }

    return this;
  }
/**
   **/
  public WlanConf scheduleEnabled(Boolean scheduleEnabled) {
    this.scheduleEnabled = scheduleEnabled;
    return this;
  }

  
  @JsonProperty("schedule_enabled")
  public Boolean getScheduleEnabled() {
    return scheduleEnabled;
  }

  @JsonProperty("schedule_enabled")
  public void setScheduleEnabled(Boolean scheduleEnabled) {
    this.scheduleEnabled = scheduleEnabled;
  }

/**
   **/
  public WlanConf scheduleReversed(Boolean scheduleReversed) {
    this.scheduleReversed = scheduleReversed;
    return this;
  }

  
  @JsonProperty("schedule_reversed")
  public Boolean getScheduleReversed() {
    return scheduleReversed;
  }

  @JsonProperty("schedule_reversed")
  public void setScheduleReversed(Boolean scheduleReversed) {
    this.scheduleReversed = scheduleReversed;
  }

/**
   **/
  public WlanConf radiusDasEnabled(Boolean radiusDasEnabled) {
    this.radiusDasEnabled = radiusDasEnabled;
    return this;
  }

  
  @JsonProperty("radius_das_enabled")
  public Boolean getRadiusDasEnabled() {
    return radiusDasEnabled;
  }

  @JsonProperty("radius_das_enabled")
  public void setRadiusDasEnabled(Boolean radiusDasEnabled) {
    this.radiusDasEnabled = radiusDasEnabled;
  }

/**
   **/
  public WlanConf hotspot2confEnabled(Boolean hotspot2confEnabled) {
    this.hotspot2confEnabled = hotspot2confEnabled;
    return this;
  }

  
  @JsonProperty("hotspot2conf_enabled")
  public Boolean getHotspot2confEnabled() {
    return hotspot2confEnabled;
  }

  @JsonProperty("hotspot2conf_enabled")
  public void setHotspot2confEnabled(Boolean hotspot2confEnabled) {
    this.hotspot2confEnabled = hotspot2confEnabled;
  }

/**
   **/
  public WlanConf bssTransition(Boolean bssTransition) {
    this.bssTransition = bssTransition;
    return this;
  }

  
  @JsonProperty("bss_transition")
  public Boolean getBssTransition() {
    return bssTransition;
  }

  @JsonProperty("bss_transition")
  public void setBssTransition(Boolean bssTransition) {
    this.bssTransition = bssTransition;
  }

/**
   **/
  public WlanConf authCache(Boolean authCache) {
    this.authCache = authCache;
    return this;
  }

  
  @JsonProperty("auth_cache")
  public Boolean getAuthCache() {
    return authCache;
  }

  @JsonProperty("auth_cache")
  public void setAuthCache(Boolean authCache) {
    this.authCache = authCache;
  }

/**
   **/
  public WlanConf radiusprofileId(String radiusprofileId) {
    this.radiusprofileId = radiusprofileId;
    return this;
  }

  
  @JsonProperty("radiusprofile_id")
  public String getRadiusprofileId() {
    return radiusprofileId;
  }

  @JsonProperty("radiusprofile_id")
  public void setRadiusprofileId(String radiusprofileId) {
    this.radiusprofileId = radiusprofileId;
  }

/**
   **/
  public WlanConf pmfMode(String pmfMode) {
    this.pmfMode = pmfMode;
    return this;
  }

  
  @JsonProperty("pmf_mode")
  public String getPmfMode() {
    return pmfMode;
  }

  @JsonProperty("pmf_mode")
  public void setPmfMode(String pmfMode) {
    this.pmfMode = pmfMode;
  }

/**
   **/
  public WlanConf bSupported(Boolean bSupported) {
    this.bSupported = bSupported;
    return this;
  }

  
  @JsonProperty("b_supported")
  public Boolean getbSupported() {
    return bSupported;
  }

  @JsonProperty("b_supported")
  public void setbSupported(Boolean bSupported) {
    this.bSupported = bSupported;
  }

/**
   **/
  public WlanConf security(SecurityEnum security) {
    this.security = security;
    return this;
  }

  
  @JsonProperty("security")
  @NotNull
  public SecurityEnum getSecurity() {
    return security;
  }

  @JsonProperty("security")
  public void setSecurity(SecurityEnum security) {
    this.security = security;
  }

/**
   **/
  public WlanConf siteId(String siteId) {
    this.siteId = siteId;
    return this;
  }

  
  @JsonProperty("site_id")
  public String getSiteId() {
    return siteId;
  }

  @JsonProperty("site_id")
  public void setSiteId(String siteId) {
    this.siteId = siteId;
  }

/**
   **/
  public WlanConf usergroupId(String usergroupId) {
    this.usergroupId = usergroupId;
    return this;
  }

  
  @JsonProperty("usergroup_id")
  @NotNull
  public String getUsergroupId() {
    return usergroupId;
  }

  @JsonProperty("usergroup_id")
  public void setUsergroupId(String usergroupId) {
    this.usergroupId = usergroupId;
  }

/**
   **/
  public WlanConf wepIdx(Integer wepIdx) {
    this.wepIdx = wepIdx;
    return this;
  }

  
  @JsonProperty("wep_idx")
  public Integer getWepIdx() {
    return wepIdx;
  }

  @JsonProperty("wep_idx")
  public void setWepIdx(Integer wepIdx) {
    this.wepIdx = wepIdx;
  }

/**
   **/
  public WlanConf wpaEnc(WpaEncEnum wpaEnc) {
    this.wpaEnc = wpaEnc;
    return this;
  }

  
  @JsonProperty("wpa_enc")
  @NotNull
  public WpaEncEnum getWpaEnc() {
    return wpaEnc;
  }

  @JsonProperty("wpa_enc")
  public void setWpaEnc(WpaEncEnum wpaEnc) {
    this.wpaEnc = wpaEnc;
  }

/**
   **/
  public WlanConf wpaMode(WpaModeEnum wpaMode) {
    this.wpaMode = wpaMode;
    return this;
  }

  
  @JsonProperty("wpa_mode")
  @NotNull
  public WpaModeEnum getWpaMode() {
    return wpaMode;
  }

  @JsonProperty("wpa_mode")
  public void setWpaMode(WpaModeEnum wpaMode) {
    this.wpaMode = wpaMode;
  }

/**
   **/
  public WlanConf xIappKey(String xIappKey) {
    this.xIappKey = xIappKey;
    return this;
  }

  
  @JsonProperty("x_iapp_key")
  public String getxIappKey() {
    return xIappKey;
  }

  @JsonProperty("x_iapp_key")
  public void setxIappKey(String xIappKey) {
    this.xIappKey = xIappKey;
  }

/**
   **/
  public WlanConf xPassphrase(String xPassphrase) {
    this.xPassphrase = xPassphrase;
    return this;
  }

  
  @JsonProperty("x_passphrase")
  @NotNull
  public String getxPassphrase() {
    return xPassphrase;
  }

  @JsonProperty("x_passphrase")
  public void setxPassphrase(String xPassphrase) {
    this.xPassphrase = xPassphrase;
  }

/**
   **/
  public WlanConf apGroupIds(List<String> apGroupIds) {
    this.apGroupIds = apGroupIds;
    return this;
  }

  
  @JsonProperty("ap_group_ids")
  public List<String> getApGroupIds() {
    return apGroupIds;
  }

  @JsonProperty("ap_group_ids")
  public void setApGroupIds(List<String> apGroupIds) {
    this.apGroupIds = apGroupIds;
  }

  public WlanConf addApGroupIdsItem(String apGroupIdsItem) {
    if (this.apGroupIds == null) {
      this.apGroupIds = new ArrayList<>();
    }

    this.apGroupIds.add(apGroupIdsItem);
    return this;
  }

  public WlanConf removeApGroupIdsItem(String apGroupIdsItem) {
    if (apGroupIdsItem != null && this.apGroupIds != null) {
      this.apGroupIds.remove(apGroupIdsItem);
    }

    return this;
  }
/**
   **/
  public WlanConf wlanBand(String wlanBand) {
    this.wlanBand = wlanBand;
    return this;
  }

  
  @JsonProperty("wlan_band")
  public String getWlanBand() {
    return wlanBand;
  }

  @JsonProperty("wlan_band")
  public void setWlanBand(String wlanBand) {
    this.wlanBand = wlanBand;
  }

/**
   **/
  public WlanConf networkconfId(String networkconfId) {
    this.networkconfId = networkconfId;
    return this;
  }

  
  @JsonProperty("networkconf_id")
  public String getNetworkconfId() {
    return networkconfId;
  }

  @JsonProperty("networkconf_id")
  public void setNetworkconfId(String networkconfId) {
    this.networkconfId = networkconfId;
  }

/**
   **/
  public WlanConf iappEnabled(Boolean iappEnabled) {
    this.iappEnabled = iappEnabled;
    return this;
  }

  
  @JsonProperty("iapp_enabled")
  public Boolean getIappEnabled() {
    return iappEnabled;
  }

  @JsonProperty("iapp_enabled")
  public void setIappEnabled(Boolean iappEnabled) {
    this.iappEnabled = iappEnabled;
  }

/**
   **/
  public WlanConf scheduleWithDuration(List<String> scheduleWithDuration) {
    this.scheduleWithDuration = scheduleWithDuration;
    return this;
  }

  
  @JsonProperty("schedule_with_duration")
  public List<String> getScheduleWithDuration() {
    return scheduleWithDuration;
  }

  @JsonProperty("schedule_with_duration")
  public void setScheduleWithDuration(List<String> scheduleWithDuration) {
    this.scheduleWithDuration = scheduleWithDuration;
  }

  public WlanConf addScheduleWithDurationItem(String scheduleWithDurationItem) {
    if (this.scheduleWithDuration == null) {
      this.scheduleWithDuration = new ArrayList<>();
    }

    this.scheduleWithDuration.add(scheduleWithDurationItem);
    return this;
  }

  public WlanConf removeScheduleWithDurationItem(String scheduleWithDurationItem) {
    if (scheduleWithDurationItem != null && this.scheduleWithDuration != null) {
      this.scheduleWithDuration.remove(scheduleWithDurationItem);
    }

    return this;
  }
/**
   **/
  public WlanConf optimizeIotWifiConnectivity(Boolean optimizeIotWifiConnectivity) {
    this.optimizeIotWifiConnectivity = optimizeIotWifiConnectivity;
    return this;
  }

  
  @JsonProperty("optimize_iot_wifi_connectivity")
  public Boolean getOptimizeIotWifiConnectivity() {
    return optimizeIotWifiConnectivity;
  }

  @JsonProperty("optimize_iot_wifi_connectivity")
  public void setOptimizeIotWifiConnectivity(Boolean optimizeIotWifiConnectivity) {
    this.optimizeIotWifiConnectivity = optimizeIotWifiConnectivity;
  }

/**
   **/
  public WlanConf dtim6e(Integer dtim6e) {
    this.dtim6e = dtim6e;
    return this;
  }

  
  @JsonProperty("dtim_6e")
  public Integer getDtim6e() {
    return dtim6e;
  }

  @JsonProperty("dtim_6e")
  public void setDtim6e(Integer dtim6e) {
    this.dtim6e = dtim6e;
  }

/**
   **/
  public WlanConf wlanBands(List<String> wlanBands) {
    this.wlanBands = wlanBands;
    return this;
  }

  
  @JsonProperty("wlan_bands")
  public List<String> getWlanBands() {
    return wlanBands;
  }

  @JsonProperty("wlan_bands")
  public void setWlanBands(List<String> wlanBands) {
    this.wlanBands = wlanBands;
  }

  public WlanConf addWlanBandsItem(String wlanBandsItem) {
    if (this.wlanBands == null) {
      this.wlanBands = new ArrayList<>();
    }

    this.wlanBands.add(wlanBandsItem);
    return this;
  }

  public WlanConf removeWlanBandsItem(String wlanBandsItem) {
    if (wlanBandsItem != null && this.wlanBands != null) {
      this.wlanBands.remove(wlanBandsItem);
    }

    return this;
  }
/**
   **/
  public WlanConf settingPreference(String settingPreference) {
    this.settingPreference = settingPreference;
    return this;
  }

  
  @JsonProperty("setting_preference")
  public String getSettingPreference() {
    return settingPreference;
  }

  @JsonProperty("setting_preference")
  public void setSettingPreference(String settingPreference) {
    this.settingPreference = settingPreference;
  }

/**
   **/
  public WlanConf minrateSettingPreference(String minrateSettingPreference) {
    this.minrateSettingPreference = minrateSettingPreference;
    return this;
  }

  
  @JsonProperty("minrate_setting_preference")
  public String getMinrateSettingPreference() {
    return minrateSettingPreference;
  }

  @JsonProperty("minrate_setting_preference")
  public void setMinrateSettingPreference(String minrateSettingPreference) {
    this.minrateSettingPreference = minrateSettingPreference;
  }

/**
   **/
  public WlanConf fastRoamingEnabled(Boolean fastRoamingEnabled) {
    this.fastRoamingEnabled = fastRoamingEnabled;
    return this;
  }

  
  @JsonProperty("fast_roaming_enabled")
  public Boolean getFastRoamingEnabled() {
    return fastRoamingEnabled;
  }

  @JsonProperty("fast_roaming_enabled")
  public void setFastRoamingEnabled(Boolean fastRoamingEnabled) {
    this.fastRoamingEnabled = fastRoamingEnabled;
  }

/**
   **/
  public WlanConf hideSsid(Boolean hideSsid) {
    this.hideSsid = hideSsid;
    return this;
  }

  
  @JsonProperty("hide_ssid")
  public Boolean getHideSsid() {
    return hideSsid;
  }

  @JsonProperty("hide_ssid")
  public void setHideSsid(Boolean hideSsid) {
    this.hideSsid = hideSsid;
  }

/**
   **/
  public WlanConf l2Isolation(Boolean l2Isolation) {
    this.l2Isolation = l2Isolation;
    return this;
  }

  
  @JsonProperty("l2_isolation")
  public Boolean getL2Isolation() {
    return l2Isolation;
  }

  @JsonProperty("l2_isolation")
  public void setL2Isolation(Boolean l2Isolation) {
    this.l2Isolation = l2Isolation;
  }

/**
   **/
  public WlanConf mcastenhanceEnabled(Boolean mcastenhanceEnabled) {
    this.mcastenhanceEnabled = mcastenhanceEnabled;
    return this;
  }

  
  @JsonProperty("mcastenhance_enabled")
  public Boolean getMcastenhanceEnabled() {
    return mcastenhanceEnabled;
  }

  @JsonProperty("mcastenhance_enabled")
  public void setMcastenhanceEnabled(Boolean mcastenhanceEnabled) {
    this.mcastenhanceEnabled = mcastenhanceEnabled;
  }

/**
   **/
  public WlanConf no2ghzOui(Boolean no2ghzOui) {
    this.no2ghzOui = no2ghzOui;
    return this;
  }

  
  @JsonProperty("no2ghz_oui")
  public Boolean getNo2ghzOui() {
    return no2ghzOui;
  }

  @JsonProperty("no2ghz_oui")
  public void setNo2ghzOui(Boolean no2ghzOui) {
    this.no2ghzOui = no2ghzOui;
  }

/**
   **/
  public WlanConf proxyArp(Boolean proxyArp) {
    this.proxyArp = proxyArp;
    return this;
  }

  
  @JsonProperty("proxy_arp")
  public Boolean getProxyArp() {
    return proxyArp;
  }

  @JsonProperty("proxy_arp")
  public void setProxyArp(Boolean proxyArp) {
    this.proxyArp = proxyArp;
  }

/**
   **/
  public WlanConf radiusMacAuthEnabled(Boolean radiusMacAuthEnabled) {
    this.radiusMacAuthEnabled = radiusMacAuthEnabled;
    return this;
  }

  
  @JsonProperty("radius_mac_auth_enabled")
  public Boolean getRadiusMacAuthEnabled() {
    return radiusMacAuthEnabled;
  }

  @JsonProperty("radius_mac_auth_enabled")
  public void setRadiusMacAuthEnabled(Boolean radiusMacAuthEnabled) {
    this.radiusMacAuthEnabled = radiusMacAuthEnabled;
  }

/**
   **/
  public WlanConf radiusMacaclFormat(String radiusMacaclFormat) {
    this.radiusMacaclFormat = radiusMacaclFormat;
    return this;
  }

  
  @JsonProperty("radius_macacl_format")
  public String getRadiusMacaclFormat() {
    return radiusMacaclFormat;
  }

  @JsonProperty("radius_macacl_format")
  public void setRadiusMacaclFormat(String radiusMacaclFormat) {
    this.radiusMacaclFormat = radiusMacaclFormat;
  }

/**
   **/
  public WlanConf saeGroups(List<String> saeGroups) {
    this.saeGroups = saeGroups;
    return this;
  }

  
  @JsonProperty("sae_groups")
  public List<String> getSaeGroups() {
    return saeGroups;
  }

  @JsonProperty("sae_groups")
  public void setSaeGroups(List<String> saeGroups) {
    this.saeGroups = saeGroups;
  }

  public WlanConf addSaeGroupsItem(String saeGroupsItem) {
    if (this.saeGroups == null) {
      this.saeGroups = new ArrayList<>();
    }

    this.saeGroups.add(saeGroupsItem);
    return this;
  }

  public WlanConf removeSaeGroupsItem(String saeGroupsItem) {
    if (saeGroupsItem != null && this.saeGroups != null) {
      this.saeGroups.remove(saeGroupsItem);
    }

    return this;
  }
/**
   **/
  public WlanConf saePsk(List<String> saePsk) {
    this.saePsk = saePsk;
    return this;
  }

  
  @JsonProperty("sae_psk")
  public List<String> getSaePsk() {
    return saePsk;
  }

  @JsonProperty("sae_psk")
  public void setSaePsk(List<String> saePsk) {
    this.saePsk = saePsk;
  }

  public WlanConf addSaePskItem(String saePskItem) {
    if (this.saePsk == null) {
      this.saePsk = new ArrayList<>();
    }

    this.saePsk.add(saePskItem);
    return this;
  }

  public WlanConf removeSaePskItem(String saePskItem) {
    if (saePskItem != null && this.saePsk != null) {
      this.saePsk.remove(saePskItem);
    }

    return this;
  }
/**
   **/
  public WlanConf uapsdEnabled(Boolean uapsdEnabled) {
    this.uapsdEnabled = uapsdEnabled;
    return this;
  }

  
  @JsonProperty("uapsd_enabled")
  public Boolean getUapsdEnabled() {
    return uapsdEnabled;
  }

  @JsonProperty("uapsd_enabled")
  public void setUapsdEnabled(Boolean uapsdEnabled) {
    this.uapsdEnabled = uapsdEnabled;
  }

/**
   **/
  public WlanConf wpa3Enhanced192(Boolean wpa3Enhanced192) {
    this.wpa3Enhanced192 = wpa3Enhanced192;
    return this;
  }

  
  @JsonProperty("wpa3_enhanced_192")
  public Boolean getWpa3Enhanced192() {
    return wpa3Enhanced192;
  }

  @JsonProperty("wpa3_enhanced_192")
  public void setWpa3Enhanced192(Boolean wpa3Enhanced192) {
    this.wpa3Enhanced192 = wpa3Enhanced192;
  }

/**
   **/
  public WlanConf wpa3FastRoaming(Boolean wpa3FastRoaming) {
    this.wpa3FastRoaming = wpa3FastRoaming;
    return this;
  }

  
  @JsonProperty("wpa3_fast_roaming")
  public Boolean getWpa3FastRoaming() {
    return wpa3FastRoaming;
  }

  @JsonProperty("wpa3_fast_roaming")
  public void setWpa3FastRoaming(Boolean wpa3FastRoaming) {
    this.wpa3FastRoaming = wpa3FastRoaming;
  }

/**
   **/
  public WlanConf wpa3Support(Boolean wpa3Support) {
    this.wpa3Support = wpa3Support;
    return this;
  }

  
  @JsonProperty("wpa3_support")
  public Boolean getWpa3Support() {
    return wpa3Support;
  }

  @JsonProperty("wpa3_support")
  public void setWpa3Support(Boolean wpa3Support) {
    this.wpa3Support = wpa3Support;
  }

/**
   **/
  public WlanConf wpa3Transition(Boolean wpa3Transition) {
    this.wpa3Transition = wpa3Transition;
    return this;
  }

  
  @JsonProperty("wpa3_transition")
  public Boolean getWpa3Transition() {
    return wpa3Transition;
  }

  @JsonProperty("wpa3_transition")
  public void setWpa3Transition(Boolean wpa3Transition) {
    this.wpa3Transition = wpa3Transition;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WlanConf wlanConf = (WlanConf) o;
    return Objects.equals(this.id, wlanConf.id) &&
        Objects.equals(this.bcFilterEnabled, wlanConf.bcFilterEnabled) &&
        Objects.equals(this.bcFilterList, wlanConf.bcFilterList) &&
        Objects.equals(this.dtimMode, wlanConf.dtimMode) &&
        Objects.equals(this.dtimNa, wlanConf.dtimNa) &&
        Objects.equals(this.dtimNg, wlanConf.dtimNg) &&
        Objects.equals(this.enabled, wlanConf.enabled) &&
        Objects.equals(this.groupRekey, wlanConf.groupRekey) &&
        Objects.equals(this.isGuest, wlanConf.isGuest) &&
        Objects.equals(this.macFilterEnabled, wlanConf.macFilterEnabled) &&
        Objects.equals(this.macFilterList, wlanConf.macFilterList) &&
        Objects.equals(this.macFilterPolicy, wlanConf.macFilterPolicy) &&
        Objects.equals(this.minrateNaAdvertisingRates, wlanConf.minrateNaAdvertisingRates) &&
        Objects.equals(this.minrateNaDataRateKbps, wlanConf.minrateNaDataRateKbps) &&
        Objects.equals(this.minrateNaEnabled, wlanConf.minrateNaEnabled) &&
        Objects.equals(this.minrateNgAdvertisingRates, wlanConf.minrateNgAdvertisingRates) &&
        Objects.equals(this.minrateNgDataRateKbps, wlanConf.minrateNgDataRateKbps) &&
        Objects.equals(this.minrateNgEnabled, wlanConf.minrateNgEnabled) &&
        Objects.equals(this.name, wlanConf.name) &&
        Objects.equals(this.schedule, wlanConf.schedule) &&
        Objects.equals(this.scheduleEnabled, wlanConf.scheduleEnabled) &&
        Objects.equals(this.scheduleReversed, wlanConf.scheduleReversed) &&
        Objects.equals(this.radiusDasEnabled, wlanConf.radiusDasEnabled) &&
        Objects.equals(this.hotspot2confEnabled, wlanConf.hotspot2confEnabled) &&
        Objects.equals(this.bssTransition, wlanConf.bssTransition) &&
        Objects.equals(this.authCache, wlanConf.authCache) &&
        Objects.equals(this.radiusprofileId, wlanConf.radiusprofileId) &&
        Objects.equals(this.pmfMode, wlanConf.pmfMode) &&
        Objects.equals(this.bSupported, wlanConf.bSupported) &&
        Objects.equals(this.security, wlanConf.security) &&
        Objects.equals(this.siteId, wlanConf.siteId) &&
        Objects.equals(this.usergroupId, wlanConf.usergroupId) &&
        Objects.equals(this.wepIdx, wlanConf.wepIdx) &&
        Objects.equals(this.wpaEnc, wlanConf.wpaEnc) &&
        Objects.equals(this.wpaMode, wlanConf.wpaMode) &&
        Objects.equals(this.xIappKey, wlanConf.xIappKey) &&
        Objects.equals(this.xPassphrase, wlanConf.xPassphrase) &&
        Objects.equals(this.apGroupIds, wlanConf.apGroupIds) &&
        Objects.equals(this.wlanBand, wlanConf.wlanBand) &&
        Objects.equals(this.networkconfId, wlanConf.networkconfId) &&
        Objects.equals(this.iappEnabled, wlanConf.iappEnabled) &&
        Objects.equals(this.scheduleWithDuration, wlanConf.scheduleWithDuration) &&
        Objects.equals(this.optimizeIotWifiConnectivity, wlanConf.optimizeIotWifiConnectivity) &&
        Objects.equals(this.dtim6e, wlanConf.dtim6e) &&
        Objects.equals(this.wlanBands, wlanConf.wlanBands) &&
        Objects.equals(this.settingPreference, wlanConf.settingPreference) &&
        Objects.equals(this.minrateSettingPreference, wlanConf.minrateSettingPreference) &&
        Objects.equals(this.fastRoamingEnabled, wlanConf.fastRoamingEnabled) &&
        Objects.equals(this.hideSsid, wlanConf.hideSsid) &&
        Objects.equals(this.l2Isolation, wlanConf.l2Isolation) &&
        Objects.equals(this.mcastenhanceEnabled, wlanConf.mcastenhanceEnabled) &&
        Objects.equals(this.no2ghzOui, wlanConf.no2ghzOui) &&
        Objects.equals(this.proxyArp, wlanConf.proxyArp) &&
        Objects.equals(this.radiusMacAuthEnabled, wlanConf.radiusMacAuthEnabled) &&
        Objects.equals(this.radiusMacaclFormat, wlanConf.radiusMacaclFormat) &&
        Objects.equals(this.saeGroups, wlanConf.saeGroups) &&
        Objects.equals(this.saePsk, wlanConf.saePsk) &&
        Objects.equals(this.uapsdEnabled, wlanConf.uapsdEnabled) &&
        Objects.equals(this.wpa3Enhanced192, wlanConf.wpa3Enhanced192) &&
        Objects.equals(this.wpa3FastRoaming, wlanConf.wpa3FastRoaming) &&
        Objects.equals(this.wpa3Support, wlanConf.wpa3Support) &&
        Objects.equals(this.wpa3Transition, wlanConf.wpa3Transition);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, bcFilterEnabled, bcFilterList, dtimMode, dtimNa, dtimNg, enabled, groupRekey, isGuest, macFilterEnabled, macFilterList, macFilterPolicy, minrateNaAdvertisingRates, minrateNaDataRateKbps, minrateNaEnabled, minrateNgAdvertisingRates, minrateNgDataRateKbps, minrateNgEnabled, name, schedule, scheduleEnabled, scheduleReversed, radiusDasEnabled, hotspot2confEnabled, bssTransition, authCache, radiusprofileId, pmfMode, bSupported, security, siteId, usergroupId, wepIdx, wpaEnc, wpaMode, xIappKey, xPassphrase, apGroupIds, wlanBand, networkconfId, iappEnabled, scheduleWithDuration, optimizeIotWifiConnectivity, dtim6e, wlanBands, settingPreference, minrateSettingPreference, fastRoamingEnabled, hideSsid, l2Isolation, mcastenhanceEnabled, no2ghzOui, proxyArp, radiusMacAuthEnabled, radiusMacaclFormat, saeGroups, saePsk, uapsdEnabled, wpa3Enhanced192, wpa3FastRoaming, wpa3Support, wpa3Transition);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WlanConf {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    bcFilterEnabled: ").append(toIndentedString(bcFilterEnabled)).append("\n");
    sb.append("    bcFilterList: ").append(toIndentedString(bcFilterList)).append("\n");
    sb.append("    dtimMode: ").append(toIndentedString(dtimMode)).append("\n");
    sb.append("    dtimNa: ").append(toIndentedString(dtimNa)).append("\n");
    sb.append("    dtimNg: ").append(toIndentedString(dtimNg)).append("\n");
    sb.append("    enabled: ").append(toIndentedString(enabled)).append("\n");
    sb.append("    groupRekey: ").append(toIndentedString(groupRekey)).append("\n");
    sb.append("    isGuest: ").append(toIndentedString(isGuest)).append("\n");
    sb.append("    macFilterEnabled: ").append(toIndentedString(macFilterEnabled)).append("\n");
    sb.append("    macFilterList: ").append(toIndentedString(macFilterList)).append("\n");
    sb.append("    macFilterPolicy: ").append(toIndentedString(macFilterPolicy)).append("\n");
    sb.append("    minrateNaAdvertisingRates: ").append(toIndentedString(minrateNaAdvertisingRates)).append("\n");
    sb.append("    minrateNaDataRateKbps: ").append(toIndentedString(minrateNaDataRateKbps)).append("\n");
    sb.append("    minrateNaEnabled: ").append(toIndentedString(minrateNaEnabled)).append("\n");
    sb.append("    minrateNgAdvertisingRates: ").append(toIndentedString(minrateNgAdvertisingRates)).append("\n");
    sb.append("    minrateNgDataRateKbps: ").append(toIndentedString(minrateNgDataRateKbps)).append("\n");
    sb.append("    minrateNgEnabled: ").append(toIndentedString(minrateNgEnabled)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    schedule: ").append(toIndentedString(schedule)).append("\n");
    sb.append("    scheduleEnabled: ").append(toIndentedString(scheduleEnabled)).append("\n");
    sb.append("    scheduleReversed: ").append(toIndentedString(scheduleReversed)).append("\n");
    sb.append("    radiusDasEnabled: ").append(toIndentedString(radiusDasEnabled)).append("\n");
    sb.append("    hotspot2confEnabled: ").append(toIndentedString(hotspot2confEnabled)).append("\n");
    sb.append("    bssTransition: ").append(toIndentedString(bssTransition)).append("\n");
    sb.append("    authCache: ").append(toIndentedString(authCache)).append("\n");
    sb.append("    radiusprofileId: ").append(toIndentedString(radiusprofileId)).append("\n");
    sb.append("    pmfMode: ").append(toIndentedString(pmfMode)).append("\n");
    sb.append("    bSupported: ").append(toIndentedString(bSupported)).append("\n");
    sb.append("    security: ").append(toIndentedString(security)).append("\n");
    sb.append("    siteId: ").append(toIndentedString(siteId)).append("\n");
    sb.append("    usergroupId: ").append(toIndentedString(usergroupId)).append("\n");
    sb.append("    wepIdx: ").append(toIndentedString(wepIdx)).append("\n");
    sb.append("    wpaEnc: ").append(toIndentedString(wpaEnc)).append("\n");
    sb.append("    wpaMode: ").append(toIndentedString(wpaMode)).append("\n");
    sb.append("    xIappKey: ").append(toIndentedString(xIappKey)).append("\n");
    sb.append("    xPassphrase: ").append(toIndentedString(xPassphrase)).append("\n");
    sb.append("    apGroupIds: ").append(toIndentedString(apGroupIds)).append("\n");
    sb.append("    wlanBand: ").append(toIndentedString(wlanBand)).append("\n");
    sb.append("    networkconfId: ").append(toIndentedString(networkconfId)).append("\n");
    sb.append("    iappEnabled: ").append(toIndentedString(iappEnabled)).append("\n");
    sb.append("    scheduleWithDuration: ").append(toIndentedString(scheduleWithDuration)).append("\n");
    sb.append("    optimizeIotWifiConnectivity: ").append(toIndentedString(optimizeIotWifiConnectivity)).append("\n");
    sb.append("    dtim6e: ").append(toIndentedString(dtim6e)).append("\n");
    sb.append("    wlanBands: ").append(toIndentedString(wlanBands)).append("\n");
    sb.append("    settingPreference: ").append(toIndentedString(settingPreference)).append("\n");
    sb.append("    minrateSettingPreference: ").append(toIndentedString(minrateSettingPreference)).append("\n");
    sb.append("    fastRoamingEnabled: ").append(toIndentedString(fastRoamingEnabled)).append("\n");
    sb.append("    hideSsid: ").append(toIndentedString(hideSsid)).append("\n");
    sb.append("    l2Isolation: ").append(toIndentedString(l2Isolation)).append("\n");
    sb.append("    mcastenhanceEnabled: ").append(toIndentedString(mcastenhanceEnabled)).append("\n");
    sb.append("    no2ghzOui: ").append(toIndentedString(no2ghzOui)).append("\n");
    sb.append("    proxyArp: ").append(toIndentedString(proxyArp)).append("\n");
    sb.append("    radiusMacAuthEnabled: ").append(toIndentedString(radiusMacAuthEnabled)).append("\n");
    sb.append("    radiusMacaclFormat: ").append(toIndentedString(radiusMacaclFormat)).append("\n");
    sb.append("    saeGroups: ").append(toIndentedString(saeGroups)).append("\n");
    sb.append("    saePsk: ").append(toIndentedString(saePsk)).append("\n");
    sb.append("    uapsdEnabled: ").append(toIndentedString(uapsdEnabled)).append("\n");
    sb.append("    wpa3Enhanced192: ").append(toIndentedString(wpa3Enhanced192)).append("\n");
    sb.append("    wpa3FastRoaming: ").append(toIndentedString(wpa3FastRoaming)).append("\n");
    sb.append("    wpa3Support: ").append(toIndentedString(wpa3Support)).append("\n");
    sb.append("    wpa3Transition: ").append(toIndentedString(wpa3Transition)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

