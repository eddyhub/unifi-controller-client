package de.jsyn.unifi.controller.model;

import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("Login")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class Login  implements Serializable {
  
  private @Valid String username;
  private @Valid String password;
  private @Valid String token;
  private @Valid Boolean rememberMe;

  /**
   **/
  public Login username(String username) {
    this.username = username;
    return this;
  }

  
  @JsonProperty("username")
  @NotNull
  public String getUsername() {
    return username;
  }

  @JsonProperty("username")
  public void setUsername(String username) {
    this.username = username;
  }

/**
   **/
  public Login password(String password) {
    this.password = password;
    return this;
  }

  
  @JsonProperty("password")
  @NotNull
  public String getPassword() {
    return password;
  }

  @JsonProperty("password")
  public void setPassword(String password) {
    this.password = password;
  }

/**
   **/
  public Login token(String token) {
    this.token = token;
    return this;
  }

  
  @JsonProperty("token")
  public String getToken() {
    return token;
  }

  @JsonProperty("token")
  public void setToken(String token) {
    this.token = token;
  }

/**
   **/
  public Login rememberMe(Boolean rememberMe) {
    this.rememberMe = rememberMe;
    return this;
  }

  
  @JsonProperty("rememberMe")
  public Boolean getRememberMe() {
    return rememberMe;
  }

  @JsonProperty("rememberMe")
  public void setRememberMe(Boolean rememberMe) {
    this.rememberMe = rememberMe;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Login login = (Login) o;
    return Objects.equals(this.username, login.username) &&
        Objects.equals(this.password, login.password) &&
        Objects.equals(this.token, login.token) &&
        Objects.equals(this.rememberMe, login.rememberMe);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username, password, token, rememberMe);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Login {\n");
    
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    rememberMe: ").append(toIndentedString(rememberMe)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

