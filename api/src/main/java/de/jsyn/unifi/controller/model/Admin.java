package de.jsyn.unifi.controller.model;

import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonTypeName;



@JsonTypeName("Admin")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaJAXRSSpecServerCodegen")
public class Admin  implements Serializable {
  
  private @Valid String adminId;
  private @Valid String deviceId;
  private @Valid String email;
  private @Valid Boolean emailAlertEnabled;
  private @Valid Integer emailAlertGroupingDelay;
  private @Valid Boolean emailAlertGroupingEnabled;
  private @Valid Boolean htmlEmailEnabled;
  private @Valid Boolean isLocal;
  private @Valid Boolean isProfessionalInstaller;
  private @Valid Boolean isSuper;
  private @Valid String lastSiteName;
  private @Valid String name;
  private @Valid Boolean requiresNewPassword;
  private @Valid Object superSitePermissions;
  private @Valid Object uiSettings;

  /**
   **/
  public Admin adminId(String adminId) {
    this.adminId = adminId;
    return this;
  }

  
  @JsonProperty("admin_id")
  @NotNull
  public String getAdminId() {
    return adminId;
  }

  @JsonProperty("admin_id")
  public void setAdminId(String adminId) {
    this.adminId = adminId;
  }

/**
   **/
  public Admin deviceId(String deviceId) {
    this.deviceId = deviceId;
    return this;
  }

  
  @JsonProperty("device_id")
  @NotNull
  public String getDeviceId() {
    return deviceId;
  }

  @JsonProperty("device_id")
  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

/**
   **/
  public Admin email(String email) {
    this.email = email;
    return this;
  }

  
  @JsonProperty("email")
  @NotNull
  public String getEmail() {
    return email;
  }

  @JsonProperty("email")
  public void setEmail(String email) {
    this.email = email;
  }

/**
   **/
  public Admin emailAlertEnabled(Boolean emailAlertEnabled) {
    this.emailAlertEnabled = emailAlertEnabled;
    return this;
  }

  
  @JsonProperty("email_alert_enabled")
  public Boolean getEmailAlertEnabled() {
    return emailAlertEnabled;
  }

  @JsonProperty("email_alert_enabled")
  public void setEmailAlertEnabled(Boolean emailAlertEnabled) {
    this.emailAlertEnabled = emailAlertEnabled;
  }

/**
   **/
  public Admin emailAlertGroupingDelay(Integer emailAlertGroupingDelay) {
    this.emailAlertGroupingDelay = emailAlertGroupingDelay;
    return this;
  }

  
  @JsonProperty("email_alert_grouping_delay")
  public Integer getEmailAlertGroupingDelay() {
    return emailAlertGroupingDelay;
  }

  @JsonProperty("email_alert_grouping_delay")
  public void setEmailAlertGroupingDelay(Integer emailAlertGroupingDelay) {
    this.emailAlertGroupingDelay = emailAlertGroupingDelay;
  }

/**
   **/
  public Admin emailAlertGroupingEnabled(Boolean emailAlertGroupingEnabled) {
    this.emailAlertGroupingEnabled = emailAlertGroupingEnabled;
    return this;
  }

  
  @JsonProperty("email_alert_grouping_enabled")
  public Boolean getEmailAlertGroupingEnabled() {
    return emailAlertGroupingEnabled;
  }

  @JsonProperty("email_alert_grouping_enabled")
  public void setEmailAlertGroupingEnabled(Boolean emailAlertGroupingEnabled) {
    this.emailAlertGroupingEnabled = emailAlertGroupingEnabled;
  }

/**
   **/
  public Admin htmlEmailEnabled(Boolean htmlEmailEnabled) {
    this.htmlEmailEnabled = htmlEmailEnabled;
    return this;
  }

  
  @JsonProperty("html_email_enabled")
  public Boolean getHtmlEmailEnabled() {
    return htmlEmailEnabled;
  }

  @JsonProperty("html_email_enabled")
  public void setHtmlEmailEnabled(Boolean htmlEmailEnabled) {
    this.htmlEmailEnabled = htmlEmailEnabled;
  }

/**
   **/
  public Admin isLocal(Boolean isLocal) {
    this.isLocal = isLocal;
    return this;
  }

  
  @JsonProperty("is_local")
  public Boolean getIsLocal() {
    return isLocal;
  }

  @JsonProperty("is_local")
  public void setIsLocal(Boolean isLocal) {
    this.isLocal = isLocal;
  }

/**
   **/
  public Admin isProfessionalInstaller(Boolean isProfessionalInstaller) {
    this.isProfessionalInstaller = isProfessionalInstaller;
    return this;
  }

  
  @JsonProperty("is_professional_installer")
  public Boolean getIsProfessionalInstaller() {
    return isProfessionalInstaller;
  }

  @JsonProperty("is_professional_installer")
  public void setIsProfessionalInstaller(Boolean isProfessionalInstaller) {
    this.isProfessionalInstaller = isProfessionalInstaller;
  }

/**
   **/
  public Admin isSuper(Boolean isSuper) {
    this.isSuper = isSuper;
    return this;
  }

  
  @JsonProperty("is_super")
  public Boolean getIsSuper() {
    return isSuper;
  }

  @JsonProperty("is_super")
  public void setIsSuper(Boolean isSuper) {
    this.isSuper = isSuper;
  }

/**
   **/
  public Admin lastSiteName(String lastSiteName) {
    this.lastSiteName = lastSiteName;
    return this;
  }

  
  @JsonProperty("last_site_name")
  public String getLastSiteName() {
    return lastSiteName;
  }

  @JsonProperty("last_site_name")
  public void setLastSiteName(String lastSiteName) {
    this.lastSiteName = lastSiteName;
  }

/**
   **/
  public Admin name(String name) {
    this.name = name;
    return this;
  }

  
  @JsonProperty("name")
  @NotNull
  public String getName() {
    return name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

/**
   **/
  public Admin requiresNewPassword(Boolean requiresNewPassword) {
    this.requiresNewPassword = requiresNewPassword;
    return this;
  }

  
  @JsonProperty("requires_new_password")
  public Boolean getRequiresNewPassword() {
    return requiresNewPassword;
  }

  @JsonProperty("requires_new_password")
  public void setRequiresNewPassword(Boolean requiresNewPassword) {
    this.requiresNewPassword = requiresNewPassword;
  }

/**
   **/
  public Admin superSitePermissions(Object superSitePermissions) {
    this.superSitePermissions = superSitePermissions;
    return this;
  }

  
  @JsonProperty("super_site_permissions")
  public Object getSuperSitePermissions() {
    return superSitePermissions;
  }

  @JsonProperty("super_site_permissions")
  public void setSuperSitePermissions(Object superSitePermissions) {
    this.superSitePermissions = superSitePermissions;
  }

/**
   **/
  public Admin uiSettings(Object uiSettings) {
    this.uiSettings = uiSettings;
    return this;
  }

  
  @JsonProperty("ui_settings")
  public Object getUiSettings() {
    return uiSettings;
  }

  @JsonProperty("ui_settings")
  public void setUiSettings(Object uiSettings) {
    this.uiSettings = uiSettings;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Admin admin = (Admin) o;
    return Objects.equals(this.adminId, admin.adminId) &&
        Objects.equals(this.deviceId, admin.deviceId) &&
        Objects.equals(this.email, admin.email) &&
        Objects.equals(this.emailAlertEnabled, admin.emailAlertEnabled) &&
        Objects.equals(this.emailAlertGroupingDelay, admin.emailAlertGroupingDelay) &&
        Objects.equals(this.emailAlertGroupingEnabled, admin.emailAlertGroupingEnabled) &&
        Objects.equals(this.htmlEmailEnabled, admin.htmlEmailEnabled) &&
        Objects.equals(this.isLocal, admin.isLocal) &&
        Objects.equals(this.isProfessionalInstaller, admin.isProfessionalInstaller) &&
        Objects.equals(this.isSuper, admin.isSuper) &&
        Objects.equals(this.lastSiteName, admin.lastSiteName) &&
        Objects.equals(this.name, admin.name) &&
        Objects.equals(this.requiresNewPassword, admin.requiresNewPassword) &&
        Objects.equals(this.superSitePermissions, admin.superSitePermissions) &&
        Objects.equals(this.uiSettings, admin.uiSettings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(adminId, deviceId, email, emailAlertEnabled, emailAlertGroupingDelay, emailAlertGroupingEnabled, htmlEmailEnabled, isLocal, isProfessionalInstaller, isSuper, lastSiteName, name, requiresNewPassword, superSitePermissions, uiSettings);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Admin {\n");
    
    sb.append("    adminId: ").append(toIndentedString(adminId)).append("\n");
    sb.append("    deviceId: ").append(toIndentedString(deviceId)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    emailAlertEnabled: ").append(toIndentedString(emailAlertEnabled)).append("\n");
    sb.append("    emailAlertGroupingDelay: ").append(toIndentedString(emailAlertGroupingDelay)).append("\n");
    sb.append("    emailAlertGroupingEnabled: ").append(toIndentedString(emailAlertGroupingEnabled)).append("\n");
    sb.append("    htmlEmailEnabled: ").append(toIndentedString(htmlEmailEnabled)).append("\n");
    sb.append("    isLocal: ").append(toIndentedString(isLocal)).append("\n");
    sb.append("    isProfessionalInstaller: ").append(toIndentedString(isProfessionalInstaller)).append("\n");
    sb.append("    isSuper: ").append(toIndentedString(isSuper)).append("\n");
    sb.append("    lastSiteName: ").append(toIndentedString(lastSiteName)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    requiresNewPassword: ").append(toIndentedString(requiresNewPassword)).append("\n");
    sb.append("    superSitePermissions: ").append(toIndentedString(superSitePermissions)).append("\n");
    sb.append("    uiSettings: ").append(toIndentedString(uiSettings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


}

